"""module that test the output against verified results
"""
import os.path
import pytest
import numpy as np
import SchroedingerModules.input as inp
import schroedingerscalc

def compare(path_sol, path_test):
    """Calculate diffenrence between two solutions of the solver

    :param path_sol: path to solution
    :type path_sol: str
    :param path_test: path to comparison
    :type path_test: str
    :return: the matrizes of the solutions subtracted from another
    :rtype: lists
    """
    path_pot_sol = os.path.join(path_sol, 'potential.dat')
    path_wavefunc_sol = os.path.join(path_sol, 'wavefuncs.dat')
    path_energies__sol = os.path.join(path_sol, 'energies.dat')
    path_exp_sol = os.path.join(path_sol, 'expvalues.dat')
    potential_sol, wavefuncs_sol, energies_sol, expvalues_sol = inp.readin_plot(
        path_pot_sol, path_wavefunc_sol, path_energies__sol, path_exp_sol
        )

    path_pot_test = os.path.join(path_test, 'potential.dat')
    path_wavefunc_test = os.path.join(path_test, 'wavefuncs.dat')
    path_energies__test = os.path.join(path_test, 'energies.dat')
    path_exp_test = os.path.join(path_test, 'expvalues.dat')
    potential_test, wavefuncs_test, energies_test, expvalues_test = inp.readin_plot(
        path_pot_test, path_wavefunc_test, path_energies__test, path_exp_test
        )

    pot_diff = potential_test - potential_sol
    pot_relerr = [np.abs((test - sol))/sol for test]
    energies_diff = energies_test - energies_sol
    expvalues_diff = expvalues_test -expvalues_sol
    
    return pot_diff, energies_diff, expvalues_diff

TESTNAMES = ['inf_pot_well',
             'fin_pot_well',
             'harm_osz',
             'asym_pot_well',
             'double_pot_well_lin',
             'double_pot_well_cspline']

@pytest.mark.parametrize("testname", TESTNAMES)
def test_solver(testname):
    """Test different Potentials and solving methods and compares them with verified results
    """
    path_test = os.path.join('testcases', testname)
    schroedingerscalc.solve(path_test)

    path_sol = os.path.join('testcases', testname, 'solutions')

    pot_diff, energies_diff, expvalues_diff = compare(path_sol, path_test)

    assert (np.all(np.abs(pot_diff) < 1e-9) and
            np.all(np.abs(energies_diff) < 1e-9) and
            np.all(np.abs(expvalues_diff) < 1e-1))
    