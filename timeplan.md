# Timeplan

## Schrödinger equation solver

- [ ] Solver for 1D time-independent schroedinger equation (solving via diagonalization for chooseable area in which the equation is discretized)
- [ ] have chooseable mass of particle
- [ ] have chooseable potential by giving points (of any number)
- [ ] chooseable interpolation for the potential (piecewise linear or natural cubic spline or polynomial(with regard to all points))
- [ ] have the program store the potential in a file named **potential.dat**
- [ ] let user choose which eigenvalues and wavefunctions to calculate (from till)
- [ ] have program store eigenvalues in a file named **energies.dat** and the *normed* wavefunctions in a file named **wavefuncs.dat**
- [ ] Have a program to calculate the expectation-values of the location \<x> and the uncertainty sigma_x = sqrt(\<x^2> - \<x>^2)
- [ ] have the expectation-values and uncertainties stored in a file named **expvalues.dat**
- [ ] have the program read in the (user)input from a file named **schrodinger.dat** (Everything in *Atomic units*)
- [ ] make it that the user can give the location of the folder through a comment-line option


## Solution plotter

- [ ] have program to read to read in the files of the SGL solver
- [ ] have program to plot the Potential and the Wavefuncs and eigenvalues 
- [ ] have program to plot the uncertainty and expectation-values
- [ ] make it possible for to user to enter the folder for the files from solver via the command line
- [ ] make it possible for the user to enter to enter additional parameters for plotting via the command line

## Unit-tests

- [ ] find senseable inputs for testing
- [ ] find solutions to compare the tests inputs to
- [ ] make tests which compare the test inputs to the references(e.g.analytical solution)
- [ ] since for the wavefunctions the solutions from the solver could be different to the analytical (because only the absolut value matters), compare the squared values instead??? **not sure if this is correct**
-[ ] the tests should run automatically after one runs **python3 -m pytest** in the top directiory of the project


## Documentation
- [ ] Create a user documentation in README.md or as part of Sphinx documentation
- [ ] Create API-documentation using Spinx