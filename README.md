# SchroedingersCalc

SchroedingerCalc is an python programm for solving the 1D-schroedinger equation for arbitary potentials aswell as plotting the solution.

## User-Documentation

### Dependencies
* Phyton 3.10 or later (earlier versions might work but have not been tested)
* NumPy
* SciPy
* Matplotlib

### Testing the programm
To the the programm run pytest on the test_schrodinger.py file by::

	$ pytest ./test_schrodinger.py

in the source directory.

### Calling the programm
The programm is called to list the possible call-arguments by using::

	$ python ./schroedingerscalc.py -h

in the directory containing the repostitory.

To use the solver call the programm by::

	$ python ./schroedingerscalc.py -solve

To use plotter call the programm by::

	$ python ./schroedingerscalc.py -plot

To give the programm a certain directory to act on use::

	$ python ./schroedingerscalc.py -d DIRECTORY [-solve] [-plot]

### Input
The programm reads in an inputfile called schrodinger.inp.
The schrodinger.inp file contains the programm instructions for the solver and has the following structure

```
mass 
xMin xMax nPoint #Minimum xMin and maximum xMax of sampling points, number of sampling points nPoint 
first last # first and last eigenvalue to include in the output
intertype #interpolation-type
n #number of interpolation points n
X Y # List of X and affialiated Y values for the potential interpolation
```

All quantities are to be given in Hartree atomic-units.
Following is an example for an calculation of an finite potential well:

```
2.0 # mass    
-2.0 2.0 1999 # xMin xMax nPoint    
1 3 # first and last eigenvalue in output    
linear # interpolation type    
6 # nr. of interpolation points and xy declarations    
-2.0 0.0    
-0.5 0.0    
-0.5 -10.0    
0.5 -10.0    
0.5 0.0    
2.0 0.0    
```

### Output

The solver creates four files containing the solution of the calculation.

* potential.dat: XY list of the discretized potential
* wavefuncs.dat: XYN list of coordinates in the first column and affiliated values for the wavefunctions in the following columns
* energies.dat: List of the eigenvalues
* expvalues.dat: First column contains location expectation value of the eigenvalues, second column containts the uncertainty1

All quantaties are given in Hartree atomic units.

With the plotting functionality creates a plot containing to subplots.
One plot containts the potential, the wavefunctions and eigenvalues, the other contains the expectation values and uncertainties.
The following figure is an example for the finite potential well

![alt text](example_figure.png "example plot")

## Developer-Documentation
Descriptions of the programm methods can be found in the developer documentation in [pdf-format](https://gitlab.com/sciproaradi/schroedinger/-/raw/main/docs/_build/latex/schroedingerscalc.pdf?ref_type=heads&inline=false) or by using make in the docs directory to build another format using sphinx.

## Authors
Jonas Müller
Niklas Krantz
