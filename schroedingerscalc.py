#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Main programm of the SchroedingersCalc project
"""
import argparse
import os.path

import SchroedingerModules.solver as sol
import SchroedingerModules.plotter as plot
import SchroedingerModules.potentialdiscretizer as pd
import SchroedingerModules.input as inp

_DESCRIPTION = """
Module to solve the 1-dimensional schroedinger eqation for a potential given in a schroedinger.inp file.
The calculated values can later be exported and plotted using the included plotter. 
"""

def solve(dic):
    """Solve the one-dimensional schroeddinger equation using the schrodinger.inp file

    :param dic: location schrodinger.inp file
    :type dic: string
    """
    filepath_schrodinger = os.path.join(dic, 'schrodinger.inp')
    input_dict = inp.readin_schrod(filepath_schrodinger)
    mass = input_dict["mass"]
    xmax = input_dict["xmax"]
    xmin = input_dict["xmin"]
    npoint = input_dict["npoint"]
    inter_type = input_dict["inter_type"]
    pot = input_dict["potential_samplingpoints"]
    first = input_dict["first"]
    last = input_dict["last"]

    delta = (xmax - xmin)/(npoint + 1)
    potential = pd.discrete_pot(pot[0], pot[1], inter_type, xmin, xmax, npoint, True, dic)
    a_val = 1/(mass*delta**2)
    ef = sol.solve_normed(a_val, potential, delta, (first-1, last), True, dic)[1]
    sol.calc(potential[0], ef, delta, True, dic)

def main():
    """Main programm
    """
    parser = argparse.ArgumentParser(description=_DESCRIPTION)
    msg = 'directory to act on'
    parser.add_argument('-d', '--directory', default='.', help=msg)
    #Arguments for the solver
    group_solve = parser.add_argument_group('Solver')
    msg = 'solve the eigenvalue problem'
    group_solve.add_argument('-solve', help=msg, action='store_true')
    #Arguments for the plotter
    group_plotter = parser.add_argument_group('Plotter')
    msg = 'plot the solutions'
    group_plotter.add_argument('-plot', help=msg, action='store_true')
    #save the plots
    msg = 'save plot'
    group_plotter.add_argument('--save', help=msg, action='store_true')
    #change scaling of wavefuncs in plot
    msg = 'change scaling of the Wavefunction in plot'
    group_plotter.add_argument('--scale', help=msg, default=1, nargs=1, metavar="scale", type=float)

    msg = 'change x max and min value'
    group_plotter.add_argument('--xlim', help=msg, nargs=2, metavar=("x_min", "x_max"), type=float)

    msg = 'change y max and min value'
    group_plotter.add_argument('--ylim', help=msg, nargs=2, metavar=("y_min", "y_max"), type=float)


    args = parser.parse_args()

    dic = args.directory
    save_bool = args.save
    solve_bool = args.solve
    plot_bool = args.plot
    xlim = args.xlim
    ylim = args.ylim
    scale = args.scale
    print("scale:")
    print(scale)
    print("using files from directory: " + str(dic))

    if solve_bool:
        print("solving the problem")
        solve(dic)
        print("finished solving!")

    if plot_bool:
        print("plotting the problem")
        filepath_pot = os.path.join(dic, 'potential.dat')
        filepath_wavefuncs = os.path.join(dic, 'wavefuncs.dat')
        filepath_energies = os.path.join(dic, 'energies.dat')
        filepath_expval = os.path.join(dic, 'expvalues.dat')

        plot.visualise(inp.readin_plot(
            filepath_pot, filepath_wavefuncs, filepath_energies, filepath_expval
            ),
                       save = save_bool, dic = '.', xrange = xlim, yrange = ylim, scale = scale)
        print("finished plotting")


if __name__ == "__main__":
    main()
