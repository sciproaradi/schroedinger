
API Documentation
=================

.. automodule:: schroedingerscalc
   :members:
   
.. automodule:: SchroedingerModules.input
   :members:

.. automodule:: SchroedingerModules.potentialdiscretizer
   :members:

.. automodule:: SchroedingerModules.solver
   :members:

.. automodule:: SchroedingerModules.plotter
   :members:

.. automodule:: test_schrodinger
   :members:
