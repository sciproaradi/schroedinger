#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Module to read in input and plotting data files
"""
import numpy as np

def readin_schrod(schrod_filename):
    """read in schrodinger.inp data for the solver

    :param fileName: name and location of file to readin
    :type fileName: string
    :return:
        mass, xMin, xMax, nPoint,
        first eigenvalue, last eigenvalue,
        interpolation type, #interpolation points, Potential
    :rtype: string for interpolation type, rest float
    """
    with open(schrod_filename, 'r', encoding="utf-8")as fp:
        lines = fp.readlines()
        data = [i.split() for i in lines]

    mass = float(data[0][0])

    xmin = float(data[1][0])
    xmax = float(data[1][1])
    npoint = int(data[1][2])

    first = int(data[2][0])
    last = int(data[2][1])

    inter_type = data[3][0]

    numintpoints = float(data[4][0])

    potential_samplingpoints = np.array(data[5:], dtype=np.float32).T

    return {"mass": mass,"xmin": xmin,"xmax": xmax,"npoint": npoint,"first": first,"last": last,"inter_type": inter_type,"numintpoints": numintpoints,"potential_samplingpoints": potential_samplingpoints}

def readin_plot(pot_fn='potential.dat',
                wf_fn = 'wavefuncs.dat',
                energies_fn = 'energies.dat',
                exp_fn = 'expvalues.dat'):
    """read in the data created by the solver needed for plotting 

    :param pot_fn: name of potntial file, defaults to 'potential.dat'
    :type pot_fn: str, optional
    :param wf_fn: name of wavefuncs file, defaults to 'wavefuncs.dat'
    :type wf_fn: str, optional
    :param energies_fn: name of energies file, defaults to 'energies.dat'
    :type energies_fn: str, optional
    :param exp_fn: name of expectation value file, defaults to 'expvalues.dat'
    :type exp_fn: str, optional
    :return:
        potential in XY-Format, wavefuncs in XNY-Format, energies in X-Format, expvalues in X-Format
    :rtype: numpy arrays
    """
    potential = np.loadtxt(pot_fn)
    wavefuncs = np.loadtxt(wf_fn)
    energies = np.loadtxt(energies_fn)
    expvalues = np.loadtxt(exp_fn)

    return potential, wavefuncs, energies, expvalues
