#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
plotter module to plot the data from the solver directly or to plot calculated data saved in a file
"""
import os.path
import numpy as np
import matplotlib.pyplot as plt

def visualise(calc_val, save = True, dic = '.', xrange = None, yrange = None, scale=1):
    """Plot the solution of the solver

    :param calc_val: _description_
    :type calc_val: _type_
    :param save: _description_, defaults to True
    :type save: bool, optional
    :param dic: _description_, defaults to '.'
    :type dic: str, optional
    :param xrange: _description_, defaults to []
    :type xrange: list, optional
    :param yrange: _description_, defaults to []
    :type yrange: list, optional
    :param scale: _description_, defaults to 1
    :type scale: int, optional
    """
    potential, wavefuncs, energies, expvalues = calc_val
    energiesvalue = np.transpose(energies)

    (ax1, ax2) = plt.subplots(1,2, sharey=True)[1]
    #left subplot
    ax1.plot(potential[:, 0], potential[:, 1], color="black" )
    ax1.plot(wavefuncs[:, 0], scale * wavefuncs[:, 1:] + energiesvalue[:], c="b")
    ax1.scatter(expvalues[:, 0], energiesvalue[:], s=50, marker="x", color="green")
    ax1.hlines(energiesvalue[:], potential[0,0], potential[-1,0], colors="gray", alpha=0.5)

    ax1.set_xlabel("x [Bohr]")
    ax1.set_ylabel("Energy [Hartree]")
    if xrange:
        ax1.set_xlim(xrange)
    if yrange:
        ax1.set_ylim(yrange)
    ax1.set_title(r"Potential, Eigenstates, $\langle x\rangle$")
    #right subplot
    ax2.scatter(expvalues[:, 1], energiesvalue[:],s=50 ,marker="+", color="green")
    ax2.hlines(energiesvalue[:], expvalues[0,1], expvalues[-1,1], colors="gray", alpha=0.5)
    ax2.set_xlabel("[Bohr]")
    ax2.set_title(r"$\sigma_x$")
    plt.show()
    if save is True:
        plt.savefig(os.path.join(dic, "graphs.pdf"), format="pdf")
