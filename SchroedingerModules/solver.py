#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
solver module to solve for eigenvalues, eigenstates, expected value and uncertainty of the location
"""
import os.path
import numpy as np
from scipy import linalg

def _solve(a_val, potential, nrange):
    """solve for eigenvalues and eigenstates of given potential and parameter a

    :param a_val: equal to 1/M*delta**2
    :type a_val: float
    :param potential: potential[1] must be Potential and potential[0] x-values
    :type potential: numpy array
    :param nrange: Range of eigenvalues and eigenstates to calculate (from, to)
    :type nrange: tupel
    :return: eigenvalues in the form [ev1, ...], eigenstates ef[:, i] is vector of i-th eigenstate
    :rtype: numpy array, numpy array
    """
    diag_el =  a_val + potential[1]
    off_el = np.full(np.shape(potential[1])[0] - 1, -a_val / 2)

    ev, ef = linalg.eigh_tridiagonal(diag_el, off_el, select = 'i', select_range = nrange)

    return ev, ef

def _norm_sq(ef, delta):
    """calculate squared norm of eigenfunction

    :param ef: eigenfunction
    :type ef: numpy array
    :param delta: distance between interpolation points
    :type delta: float
    :return: squared norm of the eigenfunction
    :rtype: float
    """
    normsquare = delta * np.sum(ef**2)
    return normsquare

def _norm(ef, delta):
    """calculate norm of eingenfunction

    :param ef: eigenfunction
    :type ef: numpy array
    :param delta: distance between interpolation points
    :type delta: float
    :return: norm of the eigenfunction
    :rtype: float
    """
    norm = _norm_sq(ef, delta)**0.5
    return norm

def solve_normed(a_val, potential, delta, nrange, save = True, dic = '.'):
    """solve for eigenvalues and normed eigenstates of given potential and parameter a

    :param a: equal to 1/mass*delta**2
    :type a: float
    :param potential: potential[1] must be Potential and potential[0] x-values
    :type potential: numpy array
    :param delta: distance between interpolation points
    :type delta: float
    :param nrange: Range of eigenvalues and eigenstates to calculate (from, to)
    :type nrange: tupel
    :param save: save ev in energies.dat and ef in wavefuncs.dat, defaults to True
    :type save: bool, optional
    :return: ascending eigenvalues, normed eigenstates ef[:, i] is vector of i-th eigenstate
    :rtype: numpy array, numpy array
    """
    ev, ef = _solve(a_val, potential, nrange)
    for column in range(len(ef.T)):
        ef[:, column] = ef[:, column] / _norm(ef[:, column], delta)

    if save:
        np.savetxt(os.path.join(dic, 'energies.dat'), ev)
        np.savetxt(os.path.join(dic, 'wavefuncs.dat'),
                    np.concatenate((potential[0, None].T, ef), axis = 1))

    return ev, ef

def _location_expect(xcoords, ef, delta):
    """calculate expectation value for location

    :param xcoords: x coordinates
    :type xcoords: numpy array
    :param ef: eigenfunction
    :type ef: numpy array
    :param delta: distance between interpolation points
    :type delta: float
    :return: expectation value of location
    :rtype: float
    """
    loc_ex = delta *  np.sum(np.multiply(xcoords, ef**2))
    return loc_ex

def _location_expect_sq(xcoords, ef, delta):
    """calculate expectation value for squared location

    :param xcoords: x coordinates
    :type xcoords: numpy array
    :param ef: eigenfunction
    :type ef: numpy array
    :param delta: distance between interpolation points
    :type delta: float
    :return: expectation value of squared location
    :rtype: float
    """
    loc_ex_sq = delta *  np.sum(np.multiply(xcoords**2, ef**2))
    return loc_ex_sq

def _uncertainty(xcoords, ef, delta):
    """calculate uncertainty of location

    :param xcoords: x coordinates
    :type xcoords: numpy array
    :param ef: eigenfunction
    :type ef: numpy array
    :param delta: distance between interpolation points
    :type delta: float
    :return: expectation value of squared location
    :rtype: float
    """
    unc = ( _location_expect_sq(xcoords, ef, delta) - _location_expect(xcoords, ef, delta)**2 )**0.5
    return unc

def calc(xcoords, ef, delta, save = True, dic = '.'):
    """calculate expectation value of location and uncertainty for multiple eigenfunctions

    :param xcoords: x coordinates
    :type xcoords: numpy array
    :param ef: eigenfunction
    :type ef: numpy array
    :param delta: distance between interpolation points
    :type delta: float
    :param save: saves vlaues to expvalues.dat if set True, defaults to True
    :type save: bool, optional
    :return: expectation values in calculation[0], uncertainty in calculation[1]
    :rtype: list
    """
    calculation = [[], []]
    for column in range(len(ef.T)):
        calculation[0].append(_location_expect(xcoords, ef[:, column], delta))
        calculation[1].append(_uncertainty(xcoords, ef[:, column], delta))

    if save:
        np.savetxt(os.path.join(dic, 'expvalues.dat'), np.transpose(calculation))
    return calculation
