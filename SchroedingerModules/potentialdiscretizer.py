"""module that converts samplepoints into discrete potential"""
import os.path
import numpy as np
from scipy import interpolate

def _interp_pot(x_samplepoints, y_samplepoints, kind):
    """interpolates potential from samplepoints

    :param x_samplepoints: x coords for potential
    :type x_samplepoints: array
    :param y_samplepoints: value of potential at corresponding x value
    :type y_samplepoints: array
    :param kind: type of interpolation
    :type kind: string
    :return: interpolated function potential(x)
    :rtype: scipy.interpolate.interpolate.interp1d
    """
    if kind in ['linear', 'nearest', 'nearest-up', 'zero']:
        potential = interpolate.interp1d(x_samplepoints, y_samplepoints, kind)
    elif kind in ('polynomial', 'barycentric'):
        potential = interpolate.BarycentricInterpolator(x_samplepoints, y_samplepoints)
    elif kind == 'cspline':
        potential = interpolate.CubicSpline(x_samplepoints, y_samplepoints, bc_type="natural")

    return potential

def discrete_pot(
        x_samplepoints, y_samplepoints, kind, xmin, xmax, npoint, write_out=True, dic = '.'
        ):
    """discretizes potential  from samplepoints using different interpolation methods

    :param x_samplepoints: x coords for potential
    :type x_samplepoints: array
    :param y_samplepoints: value of potential at corresponding x value
    :type y_samplepoints: array
    :param kind: type of interpolation
    :type kind: string
    :param xMin: minimal value  of discretiztation
    :type xMin: float
    :param xMax: minimal value  of discretiztation
    :type xMax: float
    :param nPoint: number of descrete points
    :type nPoint: integer
    :param write_out: if true will save result as potential.datw file, defaults to False
    :type write_out: bool, optional
    :return: array of discretized potential
    :rtype: (2, nPoint) numpy array
    """
    potential = _interp_pot(x_samplepoints, y_samplepoints, kind)
    xnew = np.linspace(xmin, xmax, npoint)
    discrete_potential = np.stack((xnew, potential(xnew)))
    if write_out:
        np.savetxt(os.path.join(dic, 'potential.dat'), discrete_potential.transpose())

    return discrete_potential
    